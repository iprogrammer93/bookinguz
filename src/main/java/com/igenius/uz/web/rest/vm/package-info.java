/**
 * View Models used by Spring MVC REST controllers.
 */
package com.igenius.uz.web.rest.vm;
